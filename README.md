## Environment

- Django

- Celery and Redis 

- Docker 

- Swagger


## Installation

`git clone https://gitlab.com/elizaveta.nalepova/mailing-list-service.git`


`cd mailing-list-service`


`docker-compose up --build`


## Documentation in OpenApi format can be accessed from browser

`http://0.0.0.0:8000/docs/`

`http://0.0.0.0:8000/redocs/`


## Additionals tasks

- tests

- swagger 
 
- docker-compose


## Run tests

`python manage.py test api/tests`


## Service can response on such requests:

> curl -X POST http://0.0.0.0:8000/clients -d '{"id": 1, "phone_number": "8888888888", "mobile_operator_code": "000", "tag": "123", "timezone": "RU"}'

> curl -X PATCH http://0.0.0.0:8000/clients/1 -d '{"phone_number": "0", "mobile_operator_code": "+7", "tag": "oooooo", "timezone": "RU"}'

> curl http://0.0.0.0:8000/clients/1

> curl -X DELETE http://0.0.0.0:8000/clients/1

> curl -X POST http://0.0.0.0:8000/mailings -d '{"id": 3, "timestamp_start": "2022-08-23 14:45:45.123456", "timestamp_end": "2022-08-26 14:45:45.123456", "message_text": "****some beautiful text****", "mobile_operator_code": "000", "tag": "123"}'

> curl -X PATCH http://0.0.0.0:8000/mailings/1 -d ''{"message_text": "new", "mobile_operator_code": "000", "tag": "123"}'
> '

> curl http://0.0.0.0:8000/mailings/1

> curl -X DELETE http://0.0.0.0:8000/mailings/1

> curl http://0.0.0.0:8000/stats

> curl http://0.0.0.0:8000/stats/1


