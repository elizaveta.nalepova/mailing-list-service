import time

from django.test import TestCase, RequestFactory
from os import environ
from dotenv import load_dotenv
from api.views import *

load_dotenv()


class ViewsTestCase(TestCase):
    base_url = environ.get("BASE_URL")
    url_clients = base_url + "/clients"
    url_mailings = base_url + "/mailings"
    url_stats = base_url + "/stats"

    def setUp(self):
        self.factory = RequestFactory()

    def test_client_view(self):
        body = {"id": 20, "phone_number": "8888888881", "mobile_operator_code": "000", "tag": "123", "timezone": "RU"}
        request = self.factory.post(self.url_clients, body, content_type='application/json')
        response = ClientsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 201)

        body = {"id": 20, "phone_number": "8888888881", "mobile_operator_code": "000", "tag": "123", "timezone": "RU"}
        request = self.factory.post(self.url_clients, body, content_type='application/json')
        response = ClientsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 409)

        request = self.factory.post(self.url_clients, None, content_type='application/json')
        response = ClientsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"id": 20}
        request = self.factory.post(self.url_clients, body, content_type='application/json')
        response = ClientsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"phone_number": "66666666666", "mobile_operator_code": "000", "tag": "123", "timezone": "RU"}
        request = self.factory.patch(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        request = self.factory.patch(self.url_clients, None, content_type='application/json')
        response = ClientView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"timezone": "RU"}
        request = self.factory.patch(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        request = self.factory.get(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(json.loads(response.render().content)['id'], 20)
        self.assertEqual(status, 200)

        request = self.factory.get(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 100)
        status = response.status_code
        self.assertEqual(status, 404)

        request = self.factory.delete(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        request = self.factory.delete(self.url_clients, body, content_type='application/json')
        response = ClientView.as_view()(request, 100)
        status = response.status_code
        self.assertEqual(status, 404)

    def test_mailing_view(self):
        body = {"id": 20, "timestamp_start": "2022-08-23 14:45:45.123456",
                "timestamp_end": "2022-08-26 14:45:45.123456",
                "message_text": "text", "mobile_operator_code": "999", "tag": "first"}
        request = self.factory.post(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 201)

        body = {"id": 20, "timestamp_start": "2022-08-23 14:45:45.123456",
                "timestamp_end": "2022-08-26 14:45:45.123456",
                "message_text": "text", "mobile_operator_code": "999", "tag": "first"}
        request = self.factory.post(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 409)

        request = self.factory.post(self.url_mailings, None, content_type='application/json')
        response = MailingsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"id": 20}
        request = self.factory.post(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingsView.as_view()(request)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"timestamp_start": "2022-08-23 14:45:45.123456",
                "timestamp_end": "2022-08-26 14:45:45.123456",
                "message_text": "text", "mobile_operator_code": "999", "tag": "first"}
        request = self.factory.patch(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        request = self.factory.patch(self.url_mailings, None, content_type='application/json')
        response = MailingView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 400)

        body = {"timezone": "RU"}
        request = self.factory.patch(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        body = {"timezone": "RU"}
        request = self.factory.patch(self.url_mailings, json.dumps(body, default=str), content_type='application/json')
        response = MailingView.as_view()(request, 100)
        status = response.status_code
        self.assertEqual(status, 404)

        request = self.factory.get(self.url_mailings, None, content_type='application/json')
        response = MailingView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(json.loads(response.render().content)['id'], 20)
        self.assertEqual(status, 200)

        request = self.factory.get(self.url_mailings, None, content_type='application/json')
        response = MailingView.as_view()(request, 100)
        status = response.status_code
        self.assertEqual(status, 404)

        request = self.factory.delete(self.url_mailings, None, content_type='application/json')
        response = MailingView.as_view()(request, 20)
        status = response.status_code
        self.assertEqual(status, 200)

        request = self.factory.delete(self.url_mailings, None, content_type='application/json')
        response = MailingView.as_view()(request, 100)
        status = response.status_code
        self.assertEqual(status, 404)
