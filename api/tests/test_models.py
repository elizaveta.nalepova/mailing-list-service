from django.test import TestCase
from api.models import *


class ClientTestCase(TestCase):
    def setUp(self):
        Client.objects.create(id=1, phone_number="99999999999", mobile_operator_code="999", tag="first", timezone="RU")
        Client.objects.create(id=2, phone_number="88888888888", mobile_operator_code="999", tag="first", timezone="RU")

    def test_client_saved(self):
        self.assertEqual(Client.objects.filter(id=1).count(), 1)

    def test_client_deleted(self):
        Client.objects.filter(id=1).delete()
        self.assertEqual(Client.objects.filter(id=1).count(), 0)


class MailingTestCase(TestCase):
    def setUp(self):
        Mailing.objects.create(id=1, timestamp_start="2022-08-23 14:45:45.123456",
                               timestamp_end="2022-08-23 14:45:45.123456", message_text="text",
                               mobile_operator_code="999", tag="first")
        Mailing.objects.create(id=2, timestamp_start="2022-08-23 14:45:45.123456",
                               timestamp_end="2022-08-23 14:45:45.123456", message_text="text",
                               mobile_operator_code="999", tag="first")

    def test_mailing_saved(self):
        self.assertEqual(Mailing.objects.filter(id=1).count(), 1)

    def test_mailing_deleted(self):
        Mailing.objects.filter(id=1).delete()
        self.assertEqual(Mailing.objects.filter(id=1).count(), 0)


class MessageTestCase(TestCase):
    def setUp(self):
        Client.objects.create(id=1, phone_number="99999999999", mobile_operator_code="999", tag="first", timezone="RU")
        Mailing.objects.create(id=1, timestamp_start="2022-08-23 14:45:45.123456",
                               timestamp_end="2022-08-23 14:45:45.123456", message_text="text",
                               mobile_operator_code="999", tag="first")
        Message.objects.create(id=1, timestamp_sent="2022-08-23 14:45:45.123456",
                               sent_status="created", mailing_id=Mailing.objects.get(id=1),
                               client_id=Client.objects.get(id=1))

    def test_mailing_saved(self):
        self.assertEqual(Message.objects.filter(id=1).count(), 1)
