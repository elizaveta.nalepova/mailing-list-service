from django.db import models


class Mailing(models.Model):
    id = models.BigAutoField(primary_key=True)
    timestamp_start = models.DateTimeField()
    timestamp_end = models.DateTimeField()
    message_text = models.TextField()
    mobile_operator_code = models.CharField(max_length=20)
    tag = models.TextField(max_length=50)

    def to_dict(self):
        return {'id': self.id, 'timestamp_start': self.timestamp_start, 'timestamp_end': self.timestamp_end,
                'message_text': self.message_text, 'mobile_operator_code': self.mobile_operator_code,
                'tag': self.tag}

    def __str__(self):
        return "mailing_id = {} timestamp_start = {} timestamp_end = {} mobile_operator_code = {} tag = {}".format(
            self.id, self.timestamp_start, self.timestamp_end, self.mobile_operator_code, self.tag)


class Client(models.Model):
    id = models.BigAutoField(primary_key=True)
    phone_number = models.CharField(max_length=20, unique=True)
    mobile_operator_code = models.CharField(max_length=20)
    tag = models.TextField(max_length=50)
    timezone = models.CharField(max_length=10)

    def to_dict(self):
        return {'id': self.id, 'phone_number': self.phone_number, 'mobile_operator_code': self.mobile_operator_code,
                'tag': self.tag, 'timezone': self.timezone}


class Message(models.Model):
    id = models.BigAutoField(primary_key=True)
    timestamp_sent = models.DateTimeField()
    sent_status = models.CharField(max_length=50)
    mailing_id = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return "message_id = {} timestamp_sent = {} sent_status = {} mailing_id = {} client_id = {}".format(
            self.id, self.timestamp_sent, self.sent_status, self.mailing_id.id, self.client_id.id)
