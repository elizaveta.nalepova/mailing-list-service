import time
from celery import shared_task
import requests
from os import environ
import api.models
from .models import Message, Client, Mailing
from api.utils.enums import MessageStatus
from django.db.models import Max, Q
from datetime import datetime
from django.utils import timezone
from dotenv import load_dotenv

load_dotenv()
external_api_url = environ.get("EXTERNAL_API_URL")
token = environ.get("TOKEN")


@shared_task()
def send_message_task(message_id, client_id, mailing_id):
    client = Client.objects.filter(pk=client_id).get()
    mailing = Mailing.objects.filter(pk=mailing_id).get()

    url = external_api_url + str(message_id)
    headers = {'Authorization': 'Bearer {}'.format(token)}
    body = {"id": message_id, "phone": client.phone_number, "text": mailing.message_text}

    try:
        Message.objects.filter(pk=message_id).update(sent_status=MessageStatus.SENT)
        response = requests.post(url=url, json=body, headers=headers)
        status = response.status_code
        if status == 200:
            Message.objects.filter(pk=message_id).update(sent_status=MessageStatus.DELIVERED)
        else:
            Message.objects.filter(pk=message_id).update(sent_status=MessageStatus.FAILED)
    except Exception:
        Message.objects.filter(pk=message_id).update(sent_status=MessageStatus.FAILED)


@shared_task()
def send_mail_task(mailing_id):
    mailing = Mailing.objects.filter(pk=mailing_id).get()
    now = timezone.now()
    if now > mailing.timestamp_end or all_messages_delivered(mailing_id):
        return
    else:
        clients = Client.objects.filter(Q(mobile_operator_code=mailing.mobile_operator_code) & Q(tag=mailing.tag))
        if clients.count() == 0:
            return
        for client in clients:
            try:
                message = Message.objects.get(Q(client_id=client.id) & Q(mailing_id=mailing_id))
                if not message.sent_status == MessageStatus.DELIVERED:
                    send_message_task(message.id, client.id, mailing.id)
            except api.models.Message.DoesNotExist:
                max_id = Message.objects.aggregate(Max('id'))['id__max']
                if max_id is None:
                    max_id = 0
                message = Message(id=max_id + 1, timestamp_sent=datetime.now(), sent_status=MessageStatus.CREATED,
                                  mailing_id=mailing, client_id=client)
                message.save()

                send_message_task(message.id, client.id, mailing.id)
        return send_mail_task(mailing_id)


def all_messages_delivered(mailing_id):
    all = Message.objects.filter(mailing_id=mailing_id).count()
    delivered = Message.objects.filter(
        Q(mailing_id=mailing_id) & Q(sent_status=MessageStatus.DELIVERED)).count()
    if all == delivered and all != 0:
        return True
    else:
        return False
