from enum import Enum


class MessageStatus(Enum):
    CREATED = "created"
    SENT = "sent"
    DELIVERED = "delivered"
    FAILED = "failed"

    @staticmethod
    def get_all_statuses():
        return [MessageStatus.CREATED, MessageStatus.SENT, MessageStatus.DELIVERED, MessageStatus.FAILED]
