from drf_yasg import openapi
from .serializers import MailingSerializer, ClientSerializer

bad_request_response = openapi.Response('''Request body has not enough attrs. or 
    Nothing to save - request has no body.''')
bad_request_update_response = openapi.Response('''Nothing to update - request has no body.''')

client_created_response = openapi.Response('''Client {id} was saved.''')
client_conflict_response = openapi.Response('''Client {id} is already exists.''')
client_not_found_response = openapi.Response('''Client {id} not found.''')
client_ok_response = openapi.Response('''Client {id} was updated.''')
client_get_response = openapi.Response('', ClientSerializer)
client_delete_response = openapi.Response('''Client {id} was deleted.''')

mailing_created_response = openapi.Response('''Mailing {id} was created and will start at {timestamp_start}.''')
mailing_conflict_response = openapi.Response(
    '''Mailing {id} is already exists. If you want to change mailing, use PATCH request.''')
mailing_not_found_response = openapi.Response('''Mailing {id} not found.''')
mailing_ok_response = openapi.Response('''Mailing {id} was updated.''')
mailing_get_response = openapi.Response('', MailingSerializer)
mailing_delete_response = openapi.Response('''Mailing {id} was deleted.''')

stat_ok = openapi.Response('''{"str(MAILING instance)": [{"created":int},{"sent":int},{"delivered":int},{"failed":int}], 
"str": [{str:int},{str:int},{str:int},{str:int}], ...}

str(MAILING instance) = "mailing_id = int timestamp_start = datetime timestamp_end = datetime 
mobile_operator_code = string tag = string"''')
stat_internal_error = openapi.Response('''Something went wrong.''')
