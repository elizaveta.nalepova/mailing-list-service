from rest_framework import serializers


class MailingSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    timestamp_start = serializers.DateTimeField()
    timestamp_end = serializers.DateTimeField()
    message_text = serializers.Field()
    mobile_operator_code = serializers.CharField(max_length=20)
    tag = serializers.Field()


class ClientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    phone_number = serializers.CharField(max_length=20)
    mobile_operator_code = serializers.CharField(max_length=20)
    tag = serializers.Field()
    timezone = serializers.CharField(max_length=10)
