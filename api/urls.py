from django.urls import path
from .views import *

urlpatterns = [
    path('clients', ClientsView.as_view()),  # post -- create client
    path('clients/<int:id>', ClientView.as_view()),  # patch, get, delete client

    path('mailings', MailingsView.as_view()),  # post -- create mailing
    path('mailings/<int:id>', MailingView.as_view()),  # patch, get, delete mailing

    path('stats/<int:mailing_id>', detailed_stat),  # get detailed stat
    path('stats', total_stat),  # get total stat

]
