from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import json
from api.models import Client, Mailing, Message
from .utils import utils
from datetime import datetime
from .tasks import send_mail_task
from .utils.enums import MessageStatus
from rest_framework.decorators import api_view

from .utils.swagger_docs_responses import *


class ClientsView(APIView):
    @swagger_auto_schema(
        operation_summary="Create client",
        request_body=ClientSerializer,
        responses={status.HTTP_201_CREATED: client_created_response,
                   status.HTTP_400_BAD_REQUEST: bad_request_response,
                   status.HTTP_409_CONFLICT: client_conflict_response}
    )
    def post(self, request):
        try:
            body = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            return Response(f"Nothing to save - request has no body.", status=status.HTTP_400_BAD_REQUEST)

        try:
            id = body['id']
            phone_number = body['phone_number']
            mobile_operator_code = body['mobile_operator_code']
            tag = body['tag']
            timezone = body['timezone']
        except KeyError:
            return Response(f"Request body has not enough attrs.", status=status.HTTP_400_BAD_REQUEST)
        try:
            Client.objects.get(pk=id)
        except Exception:
            client = Client(id=id, phone_number=phone_number, mobile_operator_code=mobile_operator_code, tag=tag,
                            timezone=timezone)
            client.save()
            return Response(f"Client {id} was saved.", status=status.HTTP_201_CREATED)

        return Response(f"Client {id} is already exists.", status=status.HTTP_409_CONFLICT)


class ClientView(APIView):
    @swagger_auto_schema(
        operation_summary="Update client by id",
        request_body=ClientSerializer,
        responses={status.HTTP_200_OK: client_ok_response,
                   status.HTTP_400_BAD_REQUEST: bad_request_update_response,
                   status.HTTP_404_NOT_FOUND: client_not_found_response}
    )
    def patch(self, request, id):
        try:
            body = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            return Response(f"Nothing to update - request has no body.", status=status.HTTP_400_BAD_REQUEST)

        try:
            client = Client.objects.filter(pk=id)
            obj = Client.objects.get(pk=id)
            new_phone_number = utils.get_body_attr_or_replace('phone_number', body, obj)
            new_mobile_operator_code = utils.get_body_attr_or_replace('mobile_operator_code', body, obj)
            new_tag = utils.get_body_attr_or_replace('tag', body, obj)
            new_timezone = utils.get_body_attr_or_replace('timezone', body, obj)

            client.update(phone_number=new_phone_number,
                          mobile_operator_code=new_mobile_operator_code, tag=new_tag,
                          timezone=new_timezone)
        except Exception:
            return Response(f"Client {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(f"Client {id} was updated.", status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Get client info by id",
        responses={status.HTTP_200_OK: client_get_response,
                   status.HTTP_404_NOT_FOUND: client_not_found_response}
    )
    def get(self, request, id):
        try:
            client = Client.objects.get(pk=id)
        except Exception:
            return Response(f"Client {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(client.to_dict(), status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Delete client by id",
        responses={status.HTTP_200_OK: client_delete_response,
                   status.HTTP_404_NOT_FOUND: client_not_found_response}
    )
    def delete(self, request, id):
        try:
            Client.objects.get(pk=id).delete()
        except Exception:
            return Response(f"Client {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(f"Client {id} was deleted.", status=status.HTTP_200_OK)


class MailingsView(APIView):
    @swagger_auto_schema(
        operation_summary="Create and schedule mailing",
        request_body=MailingSerializer,
        responses={status.HTTP_201_CREATED: mailing_created_response,
                   status.HTTP_409_CONFLICT: mailing_conflict_response,
                   status.HTTP_400_BAD_REQUEST: bad_request_response}
    )
    def post(self, request):
        try:
            body = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            return Response(f"Nothing to save - request has no body.", status=status.HTTP_400_BAD_REQUEST)

        try:
            id = body['id']
            timestamp_start = datetime.strptime(body['timestamp_start'], '%Y-%m-%d %H:%M:%S.%f')
            timestamp_end = datetime.strptime(body['timestamp_end'], '%Y-%m-%d %H:%M:%S.%f')
            message_text = body['message_text']
            mobile_operator_code = body['mobile_operator_code']
            tag = body['tag']
        except KeyError:
            return Response(f"Request body has not enough attrs.", status=status.HTTP_400_BAD_REQUEST)

        try:
            Mailing.objects.get(pk=id)
        except Exception:
            mailing = Mailing(id=id, timestamp_start=timestamp_start, timestamp_end=timestamp_end,
                              message_text=message_text,
                              mobile_operator_code=mobile_operator_code, tag=tag)
            mailing.save()
            send_mail_task.apply_async((mailing.id,), eta=mailing.timestamp_start,
                                       expires=mailing.timestamp_end)

            return Response(f"Mailing {id} was created and will start at {timestamp_start}.",
                            status=status.HTTP_201_CREATED)

        return Response(f"Mailing {id} is already exists. If you want to change mailing, use PATCH request.",
                        status=status.HTTP_409_CONFLICT)


class MailingView(APIView):
    @swagger_auto_schema(
        operation_summary="Update mailing by id",
        request_body=MailingSerializer,
        responses={status.HTTP_200_OK: mailing_ok_response,
                   status.HTTP_400_BAD_REQUEST: bad_request_update_response,
                   status.HTTP_404_NOT_FOUND: mailing_not_found_response}
    )
    def patch(self, request, id):
        try:
            body = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            return Response(f"Nothing to update - request has no body.", status=status.HTTP_400_BAD_REQUEST)

        try:
            mailing = Mailing.objects.filter(pk=id)
            obj = Mailing.objects.get(pk=id)
            new_timestamp_start = utils.get_body_attr_or_replace('timestamp_start', body, obj)
            new_timestamp_end = utils.get_body_attr_or_replace('timestamp_end', body, obj)
            new_message_text = utils.get_body_attr_or_replace('message_text', body, obj)
            new_mobile_operator_code = utils.get_body_attr_or_replace('mobile_operator_code', body, obj)
            new_tag = utils.get_body_attr_or_replace('tag', body, obj)

            mailing.update(timestamp_start=new_timestamp_start, timestamp_end=new_timestamp_end,
                           message_text=new_message_text, mobile_operator_code=new_mobile_operator_code, tag=new_tag)
        except Exception:
            return Response(f"Mailing {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(f"Mailing {id} was updated.", status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Get mailing by id",
        responses={status.HTTP_200_OK: mailing_get_response,
                   status.HTTP_404_NOT_FOUND: mailing_not_found_response}
    )
    def get(self, request, id):
        try:
            mailing = Mailing.objects.get(pk=id)
        except Exception:
            return Response(f"Mailing {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(mailing.to_dict(), status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Delete mailing by id",
        responses={status.HTTP_200_OK: mailing_delete_response,
                   status.HTTP_404_NOT_FOUND: mailing_not_found_response}
    )
    def delete(self, request, id):
        try:
            Mailing.objects.get(pk=id).delete()
        except Exception:
            return Response(f"Mailing {id} not found.", status=status.HTTP_404_NOT_FOUND)
        return Response(f"Mailing {id} was deleted.", status=status.HTTP_200_OK)


def get_stat(mailings):
    stat = {}
    for mailing in mailings:
        key = str(mailing)
        stat[key] = []
        for sent_status in MessageStatus.get_all_statuses():
            amount = Message.objects.filter(Q(mailing_id=mailing.id) & Q(sent_status=sent_status)).count()
            stat[key].append({sent_status.value: amount})
    return stat


@swagger_auto_schema(
    operation_summary="Get amount of messages of all mailings grouped by message statuses",
    methods=['get'],
    responses={status.HTTP_200_OK: stat_ok,
               status.HTTP_500_INTERNAL_SERVER_ERROR: stat_internal_error}
)
@api_view(('GET',))
def total_stat(request):
    mailings = Mailing.objects.filter()
    try:
        stat = get_stat(mailings)
    except:
        return Response("Something went wrong.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(stat, status=status.HTTP_200_OK)


@swagger_auto_schema(
    operation_summary="Get amount of messages of specific mailing grouped by message statuses",
    methods=['get'],
    responses={status.HTTP_200_OK: stat_ok,
               status.HTTP_500_INTERNAL_SERVER_ERROR: stat_internal_error}
)
@api_view(('GET',))
def detailed_stat(request, mailing_id):
    mailing = Mailing.objects.filter(pk=mailing_id)
    try:
        stat = get_stat(mailing)
    except:
        return Response("Something went wrong.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(stat, status=status.HTTP_200_OK)
