FROM python:3.8-slim

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV DOCKER_CONTAINER 1

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt

COPY . /code/
WORKDIR /code/

EXPOSE 8000

ENV TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI3MDQ2MzUsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImFlcmFkY3MifQ.IwQ3s_C7wzvJgO09EzT7b9dVCOl-7BmNTa5BQbQDEBw
ENV EXTERNAL_API_URL=https://probe.fbrq.cloud/v1/send/
ENV CELERY_BROKER=redis://redis:6379
ENV CELERY_BACKEND=redis://redis:6379
ENV BASE_URL=http://127.0.0.1:8000